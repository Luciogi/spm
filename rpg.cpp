#include "rpg.h"


std::string generatePassword(int length)
{
    int password_len = length;
    srand(time(0)+rand());
    std::string BigAlphabet = "ABCEFGHIJKLMNOPQRSTUVWXYZ";
    std::string SmallAlphabet = "abcdefghijklmnopqrstuvwxyz";
    std::string brakets = "[]\{}()";
    std::string special_chars = ",.<>";
    std::string alphabet;
    alphabet.append(BigAlphabet);
    alphabet.append(SmallAlphabet);
    alphabet.append(brakets);
    alphabet.append(special_chars);
    std::string password;
    for (int i = 0; i < password_len; i++) {
        int random = rand() % alphabet.size();
        password.append(alphabet, random, 1);
    }

    return password;
}
