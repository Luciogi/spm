#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_dialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include "db.h"
#include <QDebug>
#include <QClipboard>
#include "rpg.h"
#include <QInputDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Show welcome page at first launch
    ui->stackedWidget->setCurrentIndex(0);

    // Disable toolbar buttons
    disable_toolbar(&ui);
    db = new Database;

    // Select rows, not cells
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    // Make table readonly
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // mapper init
    mapper = new QDataWidgetMapper;
    mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    // connect buttons
    connect(ui->input_password_authenticate->lineEdit(),SIGNAL(returnPressed()), ui->btn_unlock, SLOT(click()));
    connect(ui->btn_open_db, SIGNAL(clicked(bool)), ui->btn_open_database, SLOT(click()));

    unlock_flag = 0;
}

MainWindow::~MainWindow()
{
    delete mapper;
    delete ui;
    delete db;
}

void MainWindow::on_btn_open_database_clicked()
{
    QString path = get_file_path_from_user();
    if( ! path.isEmpty() ){
        ui->txt_db_path->setText(path);
        db->setPath(path);
        ui->stackedWidget->setCurrentIndex(1);
        ui->input_password_authenticate->setFocus();
    }
}

void MainWindow::on_btn_create_database_clicked(){
    qDebug() << "I am create Database button";

    std::unique_ptr<Ui::Dialog> ui(new Ui::Dialog);
    std::unique_ptr<QDialog> dialog(new QDialog);

    ui->setupUi(dialog.get()); // pass raw pointer
    dialog->raise();
    dialog->setWindowTitle("Create New Database");
    dialog->activateWindow();
    int result = dialog->exec();
    if(result == 1)
    {
        QString db_path = get_save_filename_from_user();

        // Do nothing when user has not selected anything
        if(db_path.isEmpty())
        {
            return;
        }

        QString db_password = ui->input_password_new_db->password();
        // Database initialization
        db->setPassword(db_password);
        db->setPath(db_path);
        db->open();

        // Create Database
        result = db->create();
        if(result == 1)
            QMessageBox::critical(nullptr,QObject::tr("SPM error"), QObject::tr("Database initialization failed!"));
    }
}

void MainWindow::on_btn_close_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_btn_unlock_clicked()
{
    // Password Verification
    QString Password = ui->input_password_authenticate->password();

    qDebug() << "Database path: " << db->getPath(); // TEST

    if( ! Password.isEmpty() )
    {
        db->setPassword(Password);
        if(! db->authentication())
        {
            QMessageBox::critical(this, QObject::tr("SPM Error") ,QObject::tr("Incorrect Password!"));
            db->close();
            return;
        }
    }
    else
    {
        QMessageBox::information(this, QObject::tr("SPM Info") ,QObject::tr("Password can't be empty!"));
        return;
    }

    QSqlTableModel* model =  db->createModel();
    ui->tableView->setModel(model);

    ui->tableView->setColumnHidden(0,true); // Hide id column
    ui->tableView->setColumnHidden(3,true); // Hide password column
    ui->tableView->verticalHeader()->hide(); // Hide tables default id column

    // reset password input after authentication
    ui->input_password_authenticate->setPassword(NULL);

    enable_toolbar(&ui);
    ui->stackedWidget->setCurrentIndex(2);

    // set unlock flag
    unlock_flag = 1;
}

void MainWindow::on_btn_lock_db_clicked()
{
    // Close database connection
    db->close();

    // disable toolbar on lock
    disable_toolbar(&ui);

    // Disable enter password page
    ui->stackedWidget->setCurrentIndex(1);

    // Disable unlock flag
    unlock_flag = 0;
}

void disable_toolbar(Ui::MainWindow** ui)
{
    (*ui)->btn_lock_db->setDisabled(true);
    (*ui)->btn_add_db_entry->setDisabled(true);
    (*ui)->btn_edit_db_entry->setDisabled(true);
    (*ui)->btn_remove_db_entry->setDisabled(true);
    (*ui)->btn_copy_username->setDisabled(true);
    (*ui)->btn_copy_password->setDisabled(true);
    (*ui)->btn_copy_url->setDisabled(true);
    (*ui)->search_bar->setDisabled(true);
}

void enable_toolbar(Ui::MainWindow** ui)
{
    (*ui)->btn_lock_db->setDisabled(false);
    (*ui)->btn_add_db_entry->setDisabled(false);
    (*ui)->btn_edit_db_entry->setDisabled(false);
    (*ui)->btn_remove_db_entry->setDisabled(false);
    (*ui)->btn_copy_username->setDisabled(false);
    (*ui)->btn_copy_password->setDisabled(false);
    (*ui)->btn_copy_url->setDisabled(false);
    (*ui)->search_bar->setDisabled(false);
}

// Use file diaglog to get file name
QString get_file_path_from_user()
{
    QString home_dir_path = QDir::homePath();
    QString file_path = QFileDialog::getOpenFileName(nullptr,
                                                "Select Database File",
                                                home_dir_path,
                                                "(*.db)");
    return file_path;
}

// Use file diaglog to get directory name
QString get_save_filename_from_user()
{
    QString home_dir_path = QDir::homePath();
    QString dir_path = QFileDialog::
        getSaveFileName(nullptr, // Fix me
                        "Create .db File",
                        home_dir_path,
                        QObject::tr("Databases (*.db)"));
    return dir_path;
}

void MainWindow::on_btn_edit_db_entry_clicked()
{
    // Select item under cursor selection
    QItemSelectionModel* select_model =  ui->tableView->selectionModel();
    if(select_model->hasSelection())
    {
        QModelIndexList index_model_list = select_model->selectedRows();
        if( index_model_list.count() != 1 ) // Editing only one item at time
        {
            QMessageBox::information(nullptr,"SPM Info","Multiple Selections are not allowed. Select only one row.");
            return;
        }

        QModelIndex index = index_model_list.first();
        mapper->setModel(ui->tableView->model());

        mapper->addMapping(ui->input_title,1);
        mapper->addMapping(ui->input_username,2);
        mapper->addMapping(ui->input_password_edit_entry,3);
        mapper->addMapping(ui->input_url,4);
        mapper->addMapping(ui->input_notes,5);
        mapper->setCurrentIndex(index.row());

        // Switch to edit page
        edit_flag = 1;
        ui->txt_entry_title->setText("Edit Entry");
        ui->stackedWidget->setCurrentIndex(3);
    }
}

void MainWindow::on_btn_add_db_entry_clicked()
{
    // Set input fields to empty string
    ui->input_title->setText("");
    ui->input_username->setText("");
    ui->input_password_edit_entry->setText("");
    ui->input_url->setText("");
    ui->input_notes->setPlainText("");

    edit_flag = 0;

    ui->txt_entry_title->setText("Create New Entry");
    ui->stackedWidget->setCurrentIndex(3);

    ui->input_password_edit_entry->setEchoMode(QLineEdit::Password);
}

void MainWindow::on_btn_apply_clicked()
{
    if(edit_flag == 0) // when flag is OFF then insert record in DB
    {
        QString title    = ui->input_title->text();
        QString username = ui->input_username->text();
        QString password = ui->input_password_edit_entry->text();
        QString url      = ui->input_url->text();
        QString notes    = ui->input_notes->toPlainText();

        db->add_entry(title,username,password,url,notes);
        ui->stackedWidget->setCurrentIndex(2);
        ui->tableView->repaint();

    }
    else if(edit_flag == 1) // when flag is ON then edit record in DB
    {
        mapper->submit();
    }
}

void MainWindow::on_btn_remove_db_entry_clicked()
{
    QItemSelectionModel* select_model =  ui->tableView->selectionModel();
    if(select_model->hasSelection())
    {
        QModelIndexList select = select_model->selectedRows();
        for(int i=0; i < select.count(); i++)
        {
            QModelIndex index = select.at(i);
            qDebug() << "id in table:  " << index.data().toString();
            db->delete_entry(index.data().toString());
        }
        ui->tableView->repaint();
    }
}

void MainWindow::on_btn_close_entry_page_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::on_search_bar_textChanged(const QString &arg1)
{
    // QString search_txt = ui->search_bar->text().trimmed();
    QString search_txt = arg1;
    if(! search_txt.isEmpty())
    {
        QAbstractItemModel *modl = ui->tableView->model();
        QSortFilterProxyModel* proxy = new QSortFilterProxyModel(modl);
        proxy->setSourceModel(modl);
        proxy->setFilterRegExp(search_txt);
        proxy->setFilterCaseSensitivity(Qt::CaseInsensitive);
        proxy->setFilterKeyColumn(1); // Default: Filter title

        QModelIndex mi = proxy->mapFromSource(proxy->index(0,0));

        qDebug() << mi.data().toString();
        qDebug() << proxy->data(proxy->index(0,0));
        ui->tableView->setModel(proxy);
        ui->tableView->repaint();

    }
    else
    {
        // FIXME: Bad Implementation redo me
        // Recreate model when nothing to search (Hacky way)
        model = db->createModel();
        ui->tableView->setModel(model);

        ui->tableView->setColumnHidden(0,true); // Hide id column
        ui->tableView->setColumnHidden(3,true); // Hide password column
        ui->tableView->verticalHeader()->hide(); // Hide tables default id column
        ui->tableView->repaint();
    }
    qDebug() << "search cursor at:" << ui->search_bar->cursorPosition();

}

void MainWindow::on_btn_copy_username_clicked()
{
    QItemSelectionModel* select_model =  ui->tableView->selectionModel();
    if(select_model->hasSelection())
    {
        QModelIndexList index_model_list = select_model->selectedRows(2); // Select row's column 2
        if( index_model_list.count() != 1 ) // Editing only one item at time
        {
            QMessageBox::information(nullptr,"SPM Info","Multiple Selections are not allowed. Select only one row.");
            return;
        }
        else
        {
            QClipboard* clipboard = QGuiApplication::clipboard();
            QModelIndex row_index = index_model_list.at(0);
            qDebug() << "Copied Username to Clipboard" << row_index.data().toString();
            clipboard->setText(row_index.data().toString());
        }
    }
}

void MainWindow::on_btn_copy_password_clicked()
{
    QItemSelectionModel* select_model =  ui->tableView->selectionModel();
    if(select_model->hasSelection())
    {
        QModelIndexList index_model_list = select_model->selectedRows(3); // Select row's column 3
        if( index_model_list.count() != 1 ) // Editing only one item at time
        {
            QMessageBox::information(nullptr,"SPM Info","Multiple Selections are not allowed. Select only one row.");
            return;
        }
        else
        {
            QClipboard* clipboard = QGuiApplication::clipboard();
            QModelIndex row_index = index_model_list.at(0);
            qDebug() << "Copied Password to Clipboard" << row_index.data().toString();
            clipboard->setText(row_index.data().toString());
        }
    }
}

void MainWindow::on_btn_copy_url_clicked()
{
    QItemSelectionModel* select_model =  ui->tableView->selectionModel();
    if(select_model->hasSelection())
    {
        QModelIndexList index_model_list = select_model->selectedRows(4); // Select row's column 4
        if( index_model_list.count() != 1 ) // Editing only one item at time
        {
            QMessageBox::information(nullptr,"SPM Info","Multiple Selections are not allowed. Select only one row.");
            return;
        }
        else
        {
            QClipboard* clipboard = QGuiApplication::clipboard();
            QModelIndex row_index = index_model_list.at(0);
            qDebug() << "Copied URL to Clipboard" << row_index.data().toString();
            clipboard->setText(row_index.data().toString());
        }
    }
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
    QItemSelectionModel* select_model =  ui->tableView->selectionModel();
    if(select_model->hasSelection())
    {
        QModelIndexList index_model_list = select_model->selectedRows(4); // Select row's column 4
        if( index_model_list.count() != 1 ) // Editing only one item at time
        {
            QMessageBox::information(nullptr,"SPM Info","Multiple Selections are not allowed. Select only one row.");
            return;
        }
        else
        {
            mapper->setModel(ui->tableView->model());
            mapper->addMapping(ui->output_username,2);
            mapper->addMapping(ui->output_password,3);
            mapper->addMapping(ui->output_url,4);
            mapper->addMapping(ui->output_notes,5);
            mapper->setCurrentIndex(index.row());
        }
    }
    ui->output_password->setEchoMode(QLineEdit::Password);
}

int output_echo_flag=0;
void MainWindow::on_btn_show_hide_password_details_clicked()
{
    if(output_echo_flag == 0)
    {
        ui->output_password->setEchoMode(QLineEdit::Normal);
        output_echo_flag = 1;
    }
    else
    {
        ui->output_password->setEchoMode(QLineEdit::Password);
        output_echo_flag = 0;
    }
}

void MainWindow::on_btn_show_hide_password_entry_page_clicked()
{
    if(output_echo_flag == 0)
    {
        ui->input_password_edit_entry->setEchoMode(QLineEdit::Normal);
        output_echo_flag = 1;
    }
    else
    {
        ui->input_password_edit_entry->setEchoMode(QLineEdit::Password);
        output_echo_flag = 0;
    }
}

void MainWindow::on_btn_toolbar_password_generator_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);
    ui->btn_generate_password_apply->setText("Copy");
}

void MainWindow::on_btn_regenerate_clicked()
{
    int length = ui->input_password_length->text().toInt();
    std::string generated_password = generatePassword(length);

    QString password = QString::fromStdString(generated_password);

    ui->input_generate_password->lineEdit()->setText(password);
}

void MainWindow::on_btn_generate_password_apply_clicked()
{
    QString password = ui->input_generate_password->password();
    QClipboard* clipboard = QGuiApplication::clipboard();
    clipboard->setText(password);
}

void MainWindow::on_btn_generate_password_close_clicked()
{
    ui->input_generate_password->clear();
    if (unlock_flag == 0)
        ui->stackedWidget->setCurrentIndex(0);
    else if(unlock_flag == 1)
        ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::on_btn_random_password_generator_clicked()
{
    int length = QInputDialog::getInt(nullptr, "Length of Password", "Length");
    std::string password = generatePassword(length);
    ui->input_password_edit_entry->setText(QString::fromStdString(password));
}
