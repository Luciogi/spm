#ifndef DB_H
#define DB_H

#include <QtSql>
#include <QDataWidgetMapper>

class Database {
private:
    QSqlDatabase dbConnection;
    QSqlQuery* query;
    QSqlTableModel* model;
    QString connection_name;
    QString path;
    QString password;


public:
    Database();
    ~Database();

    void setPath(QString path);
    void setPassword(QString password);

    QString getPath();

    void open();
    void close();
    bool authentication();
    QSqlTableModel* createModel();
    int create();
    int add_entry(QString title, QString username,
                        QString password, QString url,
                        QString notes);
    int delete_entry(QString id);
    void refreshModel();
    int edit_entry();
};

#endif // DB_H
