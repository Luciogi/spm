﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "db.h"
#include <QDataWidgetMapper>


QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_open_database_clicked();
    void on_btn_close_clicked();
    void on_btn_unlock_clicked();
    void on_btn_lock_db_clicked();
    void on_btn_create_database_clicked();

    void on_btn_edit_db_entry_clicked();

    void on_btn_add_db_entry_clicked();
    void on_search_bar_textChanged(const QString &arg1);

    void on_btn_apply_clicked();

    void on_btn_remove_db_entry_clicked();

    void on_btn_close_entry_page_clicked();

    void on_btn_copy_username_clicked();

    void on_btn_copy_password_clicked();

    void on_btn_copy_url_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_btn_show_hide_password_details_clicked();

    void on_btn_show_hide_password_entry_page_clicked();

    void on_btn_toolbar_password_generator_clicked();

    void on_btn_regenerate_clicked();

    void on_btn_generate_password_apply_clicked();

    void on_btn_generate_password_close_clicked();

    void on_btn_random_password_generator_clicked();

private:
    Ui::MainWindow *ui;
    Database* db;
    int edit_flag;
    bool unlock_flag;
    QDataWidgetMapper* mapper;
    QSqlTableModel* model;
};

void disable_toolbar(Ui::MainWindow**);
void enable_toolbar(Ui::MainWindow**);
int create_db(QString, QString, QString);
QString get_file_path_from_user();
QString get_save_filename_from_user();
#endif // MAINWINDOW_H

