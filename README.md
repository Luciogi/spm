# spm

Simple password manager - Qt Learning Project

# Screenshots
<img src=./assets/page1.png width=800>
<img src=./assets/page2.png width=800>
<img src=./assets/page3.png width=800>
<img src=./assets/page4.png width=800>

# Dependencies
- libsqlcipher
- libKF5WidgetsAddons (kde framework 5)
- libQt5Sql (Qt5 sql plugin)

# Building
```bash
mkdir build && cd build
qmake ../
make
```
