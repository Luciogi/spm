#include "db.h"
#include <QDebug>

// Setter methods
void Database::setPath(QString path)
{
    this->path = path;
}

void Database::setPassword(QString password)
{
    this->password = password;
}

// Getter methods
QString Database::getPath()
{
    return path;
}

Database::Database()
{
    dbConnection = QSqlDatabase::addDatabase("QSQLCIPHER",connection_name);
    query = new QSqlQuery(dbConnection);
    model = new QSqlTableModel(nullptr, dbConnection);

}

Database::~Database()
{
    delete query;
    delete model;
    close();
}


// Database operation methods
void Database::open()
{
    dbConnection.setDatabaseName(path);

    if (! dbConnection.open())
    {
      qDebug() << "Error: connection with database failed"; // TEST
    }
    else
      qDebug() << "Database: connection ok"; // TEST
}

void Database::close()
{
    dbConnection.close();
}

int Database::create()
{
    open();
    QSqlQuery query(dbConnection);
    query.exec("PRAGMA KEY='" + password + "'");
    bool result = query.exec("CREATE TABLE spm_table (id INTEGER PRIMARY KEY, title TEXT,username TEXT, password TEXT, url TEXT, notes TEXT)");
    if(result != true)
    {
        dbConnection.close();
        return 1; // error value
    }
    dbConnection.close();
    return 0;
}

/* return values:
 * true -> Authentication Sucessful
 * false -> Authentication Failed
 */
bool Database::authentication()
{
    open();
    bool result = {};
    QSqlQuery query(dbConnection);
    query.exec("PRAGMA KEY=\'" + password + "\'");
    result = query.exec("SELECT * FROM sqlite_master");
    if(result == true)
    {
        qDebug() << "Authentication: Successful"; // TEST
        return true;
    }
    else
    {
        dbConnection.close();
        qDebug() << "Error: Authentication Failed"; // TEST
        return false; // error
    }
}

QSqlTableModel* Database::createModel()
{
    model->setTable("spm_table");
    model->select();

    return model;
}

int Database::add_entry(QString title, QString username,
                        QString password, QString url,
                        QString notes)
{
    title.append("'");
    title.insert(0,"'");
    username.append("'");
    username.insert(0,"'");
    password.append("'");
    password.insert(0,"'");
    url.append("'");
    url.insert(0,"'");
    notes.append("'");
    notes.insert(0,"'");

    qDebug() << "INSERT INTO spm_table VALUES(NULL ," +
                            title + "," +
                            username + "," +
                            password + "," +
                            url + "," +
                            notes + ")"; // TEST
    qDebug() << title << username << password << url << notes; // TEST

    int result = query->exec("INSERT INTO spm_table VALUES(NULL ," +
                            title + "," +
                            username + "," +
                            password + "," +
                            url + "," +
                            notes + ")");
    if(result == true)
    {
        qDebug() << "Model Refreshed"; // TEST
        refreshModel();
        return 1;
    }
    else return 0;
}

void Database::refreshModel()
{
    model->setTable("spm_table");
    model->select();
}

int Database::delete_entry(QString id)
{
    qDebug() << "DELETE FROM spm_table WHERE id=" + id;
    bool result = query->exec("DELETE FROM spm_table WHERE id=" + id);
    if(! result == true)
    {
        qDebug() << "Error: Deletion Failed";
        return 1; // error value
    }
    qDebug() << "Deletion Successful";

    model->select();
    return 0;
}
